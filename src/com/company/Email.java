package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private String address;
    Email(String address){
        this.address = set(address);
    }
    public String set(String email){
        if (!isEmailValid(email)) {
            System.out.print("Ошибка ввода email! Введите еще раз: ");
            return "null";
        }
        return email;
    }
    public String get(){
        return this.address;
    }
    public static boolean isEmailValid(String email){
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
