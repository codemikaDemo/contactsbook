package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Phone {
    private String phone;
    Phone(String phone){
        this.phone = set(phone);
    }
    public String set(String phone){
        if (!isPhoneValid(phone)) {
            System.out.print("Ошибка ввода телефона! Введите еще раз: ");
            return "null";
        }
            return phone;
    }
    public String get(){
        return this.phone;
    }
    public static boolean isPhoneValid(String phone){
        String regex = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}
